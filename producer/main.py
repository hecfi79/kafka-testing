from kafka import KafkaProducer
import datetime
import time


producer = KafkaProducer(bootstrap_servers='localhost:59354')

for i in range(1000):
    message = f"Сообщение {i + 1}: {datetime.datetime.now()}"
    print(message)
    producer.send('quickstart', value=message.encode('utf-8'))
    time.sleep(0.1)

producer.close()
