# kafka-testing

## Запуск

1. Скачать и установить [Python 3](https://www.python.org/downloads/windows/);
2. Скачать и установить [PyCharm Community Edition](https://www.jetbrains.com.cn/en-us/edu-products/download/other-PCE.html);
3. Скачать и установить [Docker Desktop](Docker-desktop);
4. Скачать и установить kafka - [confluent (zip)](https://github.com/confluentinc/cli/releases/tag/v3.42.0);
5. Распаковать скачавшийся архив, открыть cmd, перейти в корневую папку confluent через команду cd в консоли;
6. Прописать confluent local kafka start;
7. confluent local kafka topic create quickstart;
8. Выбираете свой Plaintext Ports (у меня 59354), копируете его;
9. Открываете в папках в репозитории producer/main.py и consumer/main.py через PyCharm Community и вставляете вместо 59354 свой порт;
10. Для каждого файла снизу в терминале отдельно прописывайте:

```commandline
pip install kafka
pip install kafka-python
```

11. Запускаем сначала producer/main.py, потом consumer/main.py и наблюдаем результат работы программы.
