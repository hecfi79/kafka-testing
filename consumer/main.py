from kafka import KafkaConsumer
import time


consumer = KafkaConsumer('quickstart', bootstrap_servers='localhost:59354')

message_count = 0
for _ in consumer:
    message_count += 1
    print(f"Количество прочитанных сообщений: {message_count}")
    if message_count >= 1000:
        break
    time.sleep(0.1)

consumer.close()
